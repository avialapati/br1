package com.casoftware.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.casoftware.modal.User;
import com.casoftware.service.MainService;
import com.casoftware.util.ServiceStatus;

@RestController
@RequestMapping("/main")
public class MainController {

	@Autowired
	MainService mainService;

	// for getting all users details
	@RequestMapping(value = "/getAllUsers", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllBankDetails() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<User> allUsers = mainService.getAllUsers();
			if (allUsers != null && !allUsers.isEmpty()) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allUsers);
				serviceStatus.setMessage("retrived all users successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("No Users found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("success");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}
	
	
	

}
